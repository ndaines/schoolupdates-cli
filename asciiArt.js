const asciiLogo = `
                              (                              
                     (       (((       (                    
                    (((((,  (((((  *(((((                   
            (((((   (((((  (((((((  (((((   (((((           
            ((((((((   (  (((((((((  (   ((((((((           
            ((((((( (((. (((((((((((  ((( (((((((           
    ((((((( ((((((( (((((( ((((((( (((((( ((((((( (((((((   
     (((((( ((((((( (((((((( ((( (((((((( ((((((( ((((((    
       ((    (((((/ (((((((((   ((((((((( ((((((    ((      
       ((((( (((((( (((((((((( (((((((((( (((((( (((((      
   (((((((((( ((((( ((((((((( ((((((((((( ((((( ((((((((((  
  (((((((((((( *(((( (((((((( (((((((((( ((((. (((((((((((( 
       ((((((((( ,((( ((((((( ((((((((( (((. (((((((((      
                         .(((( ((((,                        
            ((((((((((.               ,((((((((((           
                                                            
   %%%%%%        %%%%  %%%%%%%%%%%  %%%%    %%%%%    %%%%   
   %%%%%%%%      %%%% %%%%          %%%%    %%%%%    %%%    
   %%%%*%%%%%    %%%% %%%%%%%        %%%%  %%%%%%%  %%%%    
   %%%%   %%%%%  %%%%   *%%%%%%%%%    %%%%%%%% %%%%%%%%     
   %%%%     %%%%%%%%%          %%%%   %%%%%%%   %%%%%%%     
   %%%%       %%%%%%% %%%%%%  %%%%%    %%%%%%   %%%%%%      
   %%%%         %%%%%   %%%%%%%%%       %%%%     %%%% 
   
`;

module.exports = {
  asciiLogo,
};