const Configstore = require('configstore');
const inquirer = require('inquirer');

const packageJson = require('../package.json');
const config = new Configstore(packageJson.name, {});

const addEnvironmentScreen = require('./addEnvironment');
const removeEnvironmentScreen = require('./removeEnvironment');

module.exports = async function() {
  return new Promise((resolve, reject) => {
    inquirer
      .prompt([
        {
          type: 'list',
          name: 'operation',
          message: 'What do you want to do?',
          choices: [
            { value: 'add', name: 'Create Environment' },
            { value: 'remove', name: 'Delete Environment' },
          ],
          pageSize: 10,
        },
      ])
      .then(async ({ operation }) => {

        switch(operation) {
          case 'add':
            await addEnvironmentScreen();
            break;
          case 'remove':
            await removeEnvironmentScreen();
            break;
          default:
            break;
        }

        resolve();
      })
      .catch(() => {
        reject();
      })
  });
}