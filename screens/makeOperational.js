const Configstore = require('configstore');
const inquirer = require('inquirer');

const packageJson = require('../package.json');
const config = new Configstore(packageJson.name, {});

const batchFetchSchools = require('../lib/batchFetchSchools');
const schoolTable = require('../lib/schoolTable');
const moment = require('moment');

module.exports = async function(updates = [], connectionOptions) {
  const environemnts = config.get('env') || {};

  return new Promise((resolve, reject) => {
    inquirer
      .prompt([
        {
          type: 'input',
          name: 'codes',
          message: 'Please enter the school code(s)',
        },
        {
          type: 'input',
          name: 'operationalDate',
          message: 'Date Operational From (DD/MM/YYYY)',
          default: moment().add(1, 'day').format('DD/MM/YYYY'),
        },
      ])
      .then(async ({ codes = '', operationalDate = '' }) => {
        if (!codes) { return resolve(updates); }

        const schoolCodesArray = String(codes).replace(/\s/g, '').split(',');
        const results = await batchFetchSchools(schoolCodesArray, connectionOptions);

        const updatedResults = results.map(
          (items) => {
            return {
              ...items,
              changed: true,
              nonOperational: false,
              operationalDate: moment(operationalDate, 'DD/MM/YYYY').toDate().toString(),
              lastUpdated: new Date().toString(),
            }
          }
        );

        const codesUpdateSet = new Set(schoolCodesArray);
        const schoolsNotUpdated = updates.filter(({ code }) => !codesUpdateSet.has(code));

        resolve([ ...schoolsNotUpdated, ...updatedResults ]);
      })
      .catch(() => {
        reject();
      })
  });
}