const Configstore = require('configstore');
const inquirer = require('inquirer');

const packageJson = require('../package.json');
const config = new Configstore(packageJson.name, {});

const commitChanges = require('../lib/commitChanges');

const schoolsTable = require('../lib/schoolTable');

module.exports = async function(updates, connectionOptions) {
  const environemnts = config.get('env') || {};

  const schoolsToUpdate = updates
    .filter(({ changed }) => changed === true);

  console.log('\n\nSchools that will be updated:');
  schoolsTable(schoolsToUpdate);
  console.log('\n');

  return new Promise((resolve, reject) => {
    inquirer
      .prompt([
        {
          type: 'list',
          name: 'confirm',
          message: 'Are you sure?',
          choices: [
            { value: false, name: 'No' },
            { value: true, name: 'Yes' },
          ],
        },
      ])
      .then(async ({ confirm }) => {
        if (confirm) {
          await commitChanges(schoolsToUpdate, connectionOptions);
        }
        

        resolve(confirm);
      })
      .catch(() => {
        reject();
      })
  });
}