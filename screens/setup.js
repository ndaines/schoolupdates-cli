const Configstore = require('configstore');
const inquirer = require('inquirer');

const packageJson = require('../package.json');
const config = new Configstore(packageJson.name, {});

module.exports = async function() {
  return new Promise((resolve, reject) => {
    inquirer
      .prompt([
        {
          type: 'list',
          name: 'envType',
          message: 'Select the environment you wish to configure',
          choices: [
            { value: 'prod', name: 'Production' },
            { value: 'stage', name: 'Stage' },
            { value: 'dev', name: 'Development' },
          ],
        },
        {
          type: 'input',
          name: 'awsRegion',
          message: 'Please enter the AWS environment',
          default: 'ap-southeast-2',
        },
        {
          type: 'input',
          name: 'awsAccessKeyId',
          message: 'AWS Access Key Id',
          default: '',
        },
        {
          type: 'input',
          name: 'awsSecretKey',
          message: 'AWS Secret Key',
          default: '',
        },
        {
          type: 'input',
          name: 'schoolsTableName',
          message: 'Schools Table Name',
          default: '',
        },
        {
          type: 'input',
          name: 'proxyAddress',
          message: 'Proxy Address',
        }
      ])
      .then(({ envType, awsRegion, awsAccessKeyId, awsSecretKey, schoolsTableName, proxyAddress }) => {
        config.set(`env.${envType}`, {
          name: envType,
          awsRegion,
          awsAccessKeyId,
          awsSecretKey,
          schoolsTableName,
          proxyAddress: !!proxyAddress ? proxyAddress : undefined,
        });
        
        const selectedEnvironement = envType;
        resolve(selectedEnvironement);
      })
      .catch(() => {
        reject();
      })
  });
}