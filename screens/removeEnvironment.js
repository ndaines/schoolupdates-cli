const Configstore = require('configstore');
const inquirer = require('inquirer');

const packageJson = require('../package.json');
const config = new Configstore(packageJson.name, {});

module.exports = async function() {
  const environemnts = config.get('env') || {};

  return new Promise((resolve, reject) => {
    inquirer
      .prompt([
        {
          type: 'list',
          name: 'env',
          message: 'Please select the environment you wish to remove',
          choices: Object.keys(environemnts).map((key) => {
            return {
              value: key,
              name: environemnts[key].name || key,
            }
          }),
        },
      ])
      .then(({ env }) => {
        config.delete(`env.${env}`);
        resolve();
      })
      .catch(() => {
        reject();
      })
  });
}