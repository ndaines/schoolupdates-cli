const Configstore = require('configstore');
const inquirer = require('inquirer');

const packageJson = require('../package.json');
const config = new Configstore(packageJson.name, {});

module.exports = async function() {
  const environemnts = config.get('env') || {};

  return new Promise((resolve, reject) => {
    inquirer
      .prompt([
        {
          type: 'list',
          name: 'operation',
          message: 'What do you want to do?',
          choices: [
            { value: 'makeOperational', name: 'Make School(s) Operational' },
            { value: 'makeNonOperational', name: 'Make School(s) Non Operational' },
            
            new inquirer.Separator(),

            { value: 'getCurrent', name: 'Reset (load data from server)' },

            new inquirer.Separator(),

            { value: 'commit', name: 'Commit Changes (Send Notifications)' },

            new inquirer.Separator(),
            
            { value: 'settings', name: 'Settings' },
          ],
          pageSize: 10,
        },
      ])
      .then(({ operation }) => {
        resolve(operation);
      })
      .catch(() => {
        reject();
      })
  });
}