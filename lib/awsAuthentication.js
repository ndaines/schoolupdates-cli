const tough = require('tough-cookie');
const prompt = require('prompt-sync')({ sigint: true });
const queryString = require('query-string');
const jsdom = require("jsdom");
const xml2js = require('xml2js');
const AWS = require('aws-sdk');
const inquirer = require('inquirer');

const { JSDOM } = jsdom;

function getEnvironmentConfig(env) {
  let idpurl;
  let oambaseurl;

  switch (env) {
    case 'uat':
      idpurl = 'https://fs.uat.det.nsw.edu.au/adfs/ls/';
      oambaseurl = 'https://saml.test.det.nsw.edu.au';
      break;

    case 'pre':
      idpurl = 'https://fs.pre.det.nsw.edu.au/adfs/ls/';
      oambaseurl = 'https://saml.pre.det.nsw.edu.au';
      break;

    case 'prod':
    default:
      idpurl = 'https://fs.det.nsw.edu.au/adfs/ls/';
      oambaseurl = 'https://saml.det.nsw.edu.au';
  }

  return {
    idpurl,
    idpentryurl: idpurl + 'IdpInitiatedSignOn.aspx?loginToRp=urn:amazon:webservices',
    oambaseurl,
  }
}

function getQueryParamsFromUrl(urlString = '') {
  if (urlString.indexOf('?') <= 0) return;
  const urlStringSearchParams = urlString.slice(urlString.indexOf('?'));
  return queryString.parse(urlStringSearchParams);
}

function elementByName(document, name) {
  const elements = document.getElementsByName(name);
  if (elements.length <= 0) return '';
  return elements[0].value;
}

module.exports = async function ({ env /* proxy agent, etc */ } = {}) {
  /**
   * IMPORTANT!!!
   * Fetch is declared in the scope of the function so that the cookie store doesn't
   * linger around after the execution of this script.
   */
  const fetch = require('fetch-cookie/node-fetch')(require('node-fetch'), new tough.CookieJar());

  const { idpurl, idpentryurl, oambaseurl } = getEnvironmentConfig(env);

  const username = prompt("Username: ");
  const password = prompt("Password: ", { echo: '*' });

  const idpAuthFormSubmitUrl = (
    await fetch (idpentryurl, { method: 'GET' })
      .then(res => res.url)
  );

  const idpparams = getQueryParamsFromUrl(idpAuthFormSubmitUrl);
  const updatedGoToPath = `/sso${idpparams.goto}`;

  const baseOamHeaders = {
    'Accept-API-Version': 'resource=2.0, protocol=1', 
    'Content-Type': 'application/json',
    'cache-control': 'no-cache'
  };

  // 1. Authenticate with OpenAM
  const oamAuthenticateUrl = oambaseurl + '/sso/json/realms/root/authenticate';
  const oamAuthenticateQuery = new URLSearchParams({
    forward: idpparams.forward,
    spEntityID: idpparams.spEntityID,
    goto: updatedGoToPath,
    AMAuthCookie: '',
  });
  const oamResponse = await fetch(oamAuthenticateUrl + '?' + oamAuthenticateQuery.toString(), {
    method: 'POST',
    headers: {
      ...baseOamHeaders,
      'X-OpenAM-Password': password,
      'X-OpenAM-Username': username,
    },
  });
  const oamAuthenticateJSONResponse = await oamResponse.json();
  
  // Authenticated Token
  const oamToken = oamAuthenticateJSONResponse.tokenId

  
  // 2. Get SAML Assertion
  const oamUrl5 = oambaseurl + updatedGoToPath;
  const oamUrl5Response = await fetch(oamUrl5, {
    method: 'GET',
    headers: {
      ...baseOamHeaders,
      'iPlanetDirectoryPro': oamToken,
      'Accept': 'text/html,application/xhtml+xml,application/xml;q=0.9,*/*;q=0.8',
    }
  });
  const oamUrl5BodyResponse = await oamUrl5Response.text();
  const oamDom = new JSDOM(oamUrl5BodyResponse);
  const oamDocument = oamDom.window.document;

  const oamSamlResponse = elementByName(oamDocument, 'SAMLResponse');
  const oamRelayState = elementByName(oamDocument, 'RelayState');
  

  // 3. Get SAML Assertion from Active Directory
  const adPayload = new URLSearchParams({
    SAMLResponse: oamSamlResponse,
    RelayState: oamRelayState,
  });

  const adResponse = await fetch(idpurl, {
    method: 'POST',
    body: adPayload,
  });

  const adBodyResponse = await adResponse.text();
  const adDom = new JSDOM(adBodyResponse);
  const adDocument = adDom.window.document;
  
  const adSamlResponse = elementByName(adDocument, 'SAMLResponse');
  
  
  // 4. Authenticate AWS Session with SAML
  const AWSPayload = new URLSearchParams({
    SAMLResponse: adSamlResponse,
  });

  const awsFormSubmitUrl = 'https://signin.aws.amazon.com/saml';
  const awsResponse = await fetch(awsFormSubmitUrl, {
    method: 'POST',
    body: AWSPayload,
  })

  const awsBodyResponse = await awsResponse.text();
  const awsDom = new JSDOM(awsBodyResponse);
  const awsDocument = awsDom.window.document;

  const awsSamlResponse = elementByName(awsDocument, 'SAMLResponse');

  // 5. Retrieve the authorised AWS Roles from the SAML Accertion
  const root = await xml2js.parseStringPromise((Buffer.from(awsSamlResponse, 'base64').toString()));
  const attributeStatement = root['samlp:Response']['Assertion'][0]['AttributeStatement'][0]['Attribute'];
  const rolesAttribute = attributeStatement.find((attr) => attr['$']['Name'] === 'https://aws.amazon.com/SAML/Attributes/Role');
  
  const awsRoles = [
    ...rolesAttribute['AttributeValue'].reduce((acc, val) => {
      const [principal_arn, role_arn] = val.split(',');
      return [
        ...acc,
        {
          role_arn,
          principal_arn,
        }
      ]
    }, [])
  ];
  
  
  // Automatically select the first item if there is only 1 role
  // Otherwise ask the user to select the key they wish

  let selectedRole = undefined;

  if (awsRoles.length <= 0) {
    // throw an error
  }
  
  const samlAccountEls = awsDocument.getElementsByClassName('saml-account-name');
  const samlAccounts = [];

  for (let i = 0; i < samlAccountEls.length; i++) {
    const elementText = samlAccountEls.item(i).textContent;
    const [pre, elName] = elementText.split(': ');
    samlAccounts.push({
      name: elName,
      id: elName.match(/\(([^}]+)\)/)[1],
    })
  }


  const updatedAWSRoles = awsRoles.map((role) => {
    const accountId = role.role_arn.match(/arn:aws:iam::([^}]+):role/)[1];
    const samlAccount = samlAccounts.find((samlItem) => samlItem.id === accountId);

    return {
      ...role,
      name: !!samlAccount ? samlAccount.name : undefined,
    }
  })

  if (awsRoles.length === 1) {
    // select first item and continue
    selectedRole = awsRoles[0];
  } else {
    // ask the user to pick an account

    console.log('\n\n');

    const selectedOptionPromise = new Promise((resolve) => {
      inquirer
        .prompt([
          {
            type: 'list',
            name: 'role',
            message: 'Please select an account from the list:',
            choices: [
              ...updatedAWSRoles.map((item) => ({
                value: item,
                name: item.name || item.role_arn,
              }))
            ],
            pageSize: 10,
          }
        ])
        .then(({ role }) => {
          resolve(role);
        })
        .then(() => {
          resolve(undefined);
        });
    });

    selectedRole = await selectedOptionPromise;
  }
  
  if (!!selectedRole) {
    // TODO: Show an error saying no choice was made
  }
  
  // 5. Generate token based on the SAML status
  const sts = new AWS.STS();

  const assumeRolePromise = new Promise((resolve) => {
    const stsParams = {
      DurationSeconds: 3600, 
      PrincipalArn: selectedRole.principal_arn, 
      RoleArn: selectedRole.role_arn, 
      SAMLAssertion: awsSamlResponse,
    };

    sts.assumeRoleWithSAML(stsParams, function (err, data) {
      console.log(data);

      if (err) {
        resolve({}); 
      } else {
        resolve({
          accessKeyId: data.Credentials.AccessKeyId,
          secretAccessKey: data.Credentials.SecretAccessKey,
          sessionToken: data.Credentials.SessionToken,
        });
      }
    });
  });

  // Return the auth credentials
  const { accessKeyId, secretAccessKey, sessionToken } = await assumeRolePromise;
  return {
    accessKeyId,
    secretAccessKey,
    sessionToken,
    name: selectedRole.name || selectedRole.role_arn
  };
}