const moment = require('moment');
const { Table } = require('console-table-printer');

const CHANGED = 'Changed';
const CODE = 'Code';
const SCHOOL_NAME = 'School Name';
const NON_OPERATIONAL = 'Non Operational';
const OPERATIONAL_DATE = 'Operational Date';
const LAST_UPDATED = 'Last Updated';

module.exports = function (schools = []) {
  const sortedSchools = schools
    .sort((a,b) => a.code - b.code)
    .sort((x,y) => (x.changed === y.changed) ? 0 : x.changed ? -1 : 1)

  const table = new Table({
    columns: [
      { name: CHANGED, alignment: 'left' },
      { name: CODE, alignment: 'left' },
      { name: SCHOOL_NAME, alignment: 'left' },
      { name: NON_OPERATIONAL, alignment: 'left' },
      { name: OPERATIONAL_DATE, alignment: 'left' },
      { name: LAST_UPDATED, alignment: 'left'},
    ]
  });

  const formattedSchools = sortedSchools.map(
    ({ changed, code, name, lastUpdated, nonOperational, operationalDate }) => {
      return {
        [CHANGED]: changed ? '*' : '',
        [CODE]: code,
        [SCHOOL_NAME]: name,
        [NON_OPERATIONAL]: nonOperational ? 'YES' : '',
        [OPERATIONAL_DATE]: moment(new Date(operationalDate)).format('ddd DD MMMM YYYY'),
        [LAST_UPDATED]: moment(new Date(lastUpdated)).format('ddd DD MMMM YYYY'),
      }
    }
  );

  table.addRows(formattedSchools);

  table.printTable();
}