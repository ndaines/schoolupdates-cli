const AWS = require('aws-sdk');
const proxy = require('proxy-agent');

module.exports = async function ({ accessKeyId, accessKeySecret, sessionToken, region, proxyAddress, tableName }) {
  const options = {
    accessKeyId: accessKeyId,
    secretAccessKey: accessKeySecret,
    sessionToken: sessionToken,
    region: region,
  };

  if (proxyAddress) {
    options.httpOptions = { agent: proxy(proxyAddress) }
  }

  AWS.config.update(options);

  const dynamodb = new AWS.DynamoDB.DocumentClient();

  const params = {
    TableName: tableName,
  };

  const scanResponse = await dynamodb.scan(params).promise();
  const { Count = 0, Items = [] } = scanResponse;

  return scanResponse;
}
