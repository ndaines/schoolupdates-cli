const AWS = require('aws-sdk');
const proxy = require('proxy-agent');

function chunk(array, size) {
  const chunked_arr = [];
  let copied = [...array]; // ES6 destructuring
  const numOfChild = Math.ceil(copied.length / size); // Round up to the nearest integer
  for (let i = 0; i < numOfChild; i++) {
    chunked_arr.push(copied.splice(0, size));
  }
  return chunked_arr;
}

module.exports = async function (codes = [], { accessKeyId, accessKeySecret, sessionToken, region, proxyAddress, tableName }) {
  const options = {
    accessKeyId: accessKeyId,
    secretAccessKey: accessKeySecret,
    sessionToken: sessionToken,
    region: region,
  };

  if (proxyAddress) {
    options.httpOptions = { agent: proxy(proxyAddress) }
  }

  AWS.config.update(options);

  const dynamodb = new AWS.DynamoDB.DocumentClient();


  const schoolChunks = chunk(codes, 80);

  const responses = await Promise.all(schoolChunks.map(
    async (chunk) => {
      const params = {
        RequestItems: {
          [tableName]: {
            Keys: chunk.map((code) => ({ code })),
          }
        }
      };
    
      const schoolResponse = await dynamodb.batchGet(params).promise();
      const result = schoolResponse && schoolResponse.Responses && schoolResponse.Responses[tableName];

      return result || [];
    }
  ));

  return responses.reduce((acc, val) => {
    return [ ...acc, ...val ];
  }, []);
}
