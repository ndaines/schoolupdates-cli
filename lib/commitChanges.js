const AWS = require('aws-sdk');
const proxy = require('proxy-agent');

module.exports = async function (schools = [], { accessKeyId, accessKeySecret, sessionToken, region, proxyAddress, tableName }) {
  const options = {
    accessKeyId: accessKeyId,
    secretAccessKey: accessKeySecret,
    sessionToken: sessionToken,
    region: region,
  };

  if (proxyAddress) {
    options.httpOptions = { agent: proxy(proxyAddress) }
  }

  AWS.config.update(options);

  const dynamodb = new AWS.DynamoDB.DocumentClient();

  const results = await Promise.all(schools.map(
    async (item) => {
      const params = {
        TableName: tableName,
        Key: {
          "code": item.code,
        },
        UpdateExpression: "set nonOperational = :x, operationalDate = :y, lastUpdated = :z",
        ExpressionAttributeValues: {
          ":x": item.nonOperational,
          ":y": item.operationalDate,
          ":z": item.lastUpdated,
        }
      }

      const result = await dynamodb.update(params).promise();

    }
  ));

  return results;
}
