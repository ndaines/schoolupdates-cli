(async function () {
  const tough = require('tough-cookie');
  const fetch = require('fetch-cookie/node-fetch')(require('node-fetch'), new tough.CookieJar());
  const prompt = require('prompt-sync')({ sigint: true });
  const queryString = require('query-string');
  const jsdom = require("jsdom");
  const { JSDOM } = jsdom;
  const xml2js = require('xml2js');
  const AWS = require('aws-sdk');

  const myenv = 'prod';

  let idpurl;
  let oambaseurl;

  switch (myenv) {
    case 'uat':
      idpurl = 'https://fs.uat.det.nsw.edu.au/adfs/ls/';
      oambaseurl = 'https://saml.test.det.nsw.edu.au';
      break;

    case 'pre':
      idpurl = 'https://fs.pre.det.nsw.edu.au/adfs/ls/';
      oambaseurl = 'https://saml.pre.det.nsw.edu.au';
      break;

    case 'prod':
    default:
      idpurl = 'https://fs.det.nsw.edu.au/adfs/ls/';
      oambaseurl = 'https://saml.det.nsw.edu.au';
  }

  // console.log("ADFS: ", idpurl);
  // console.log("OpenAM: " + oambaseurl);

  const idpentryurl = idpurl + 'IdpInitiatedSignOn.aspx?loginToRp=urn:amazon:webservices';
  const profilename = 'saml';

  const username = prompt("Username: ");
  const password = prompt("Password: ", { echo: '*' });

  const idpauthformsubmiturl = await fetch(idpentryurl, {
    method: 'GET',
  }).then(res => res.url);
  console.debug(`DEBUG: idpauthformsubmiturl is: ${idpauthformsubmiturl}`);

  function getQueryParamsFromUrl(urlString = '') {
    const urlStringSearchParams = urlString.indexOf('?') > 0 
      ? urlString.slice(urlString.indexOf('?'))
      : '';

    return queryString.parse(urlStringSearchParams)
  }
    
  // console.log('====');
  // console.log(idpauthformsubmiturl);
  // console.log('====');

  const idpparams = getQueryParamsFromUrl(idpauthformsubmiturl);

  console.log(idpparams);

  // console.log('idpparams', idpparams);

  // console.log('---goto');
  // console.log(idpparams.goto);
  // console.log('%%%')
  // console.log(encodeURIComponent(idpparams.goto));
  // console.log('---');

  const updatedGoToPath = `/sso${idpparams.goto}`;
  // console.log('updatedGoToPath', updatedGoToPath);

  const oamUrl = oambaseurl + '/sso/json/realms/root/authenticate';
  const oamUrlQuery = new URLSearchParams({
    forward: idpparams.forward,
    spEntityID: idpparams.spEntityID,
    goto: updatedGoToPath,
    AMAuthCookie: '',
  });
  // oamUrlQuery.append('forward', idpparams.forward);
  // oamUrlQuery.append('spEntityID', idpparams.spEntityID);
  // oamUrlQuery.append('goto', updatedGoToPath);
  // oamUrlQuery.append('AMAuthCookie', '');

  const oamUrlHeaders = {
    'Accept-API-Version': 'resource=2.0, protocol=1', 
    'Content-Type': 'application/json', 
    'X-OpenAM-Password': password,
    'X-OpenAM-Username': username,
    'cache-control': 'no-cache'
  }

  const oamResponse = await fetch(oamUrl + '?' + oamUrlQuery.toString(), {
    method: 'POST',
    // body: oamUrlQuery,
    headers: oamUrlHeaders,
  });

  const oamJSONResponse = await oamResponse.json();
  // console.log(oamJSONResponse);

  const oamToken = oamJSONResponse.tokenId;

  const baseOamAuthenticatedHeaders = {
    'iPlanetDirectoryPro': oamToken,
    'Accept-API-Version': 'resource=2.0, protocol=1.0',
    'Content-Type': 'application/json',
    'Accept': 'application/json, text/javascript, */*; q=0.01',
    'cache-control': 'no-cache',
  };

  // I don't think this session is required
  // const oamUrl2 = oambaseurl + '/sso/json/realms/root/users';
  // fetch(oamUrl2 + '?_action=idFromSession', {
  //   method: 'POST',
  //   headers: {
  //     ...baseOamAuthenticatedHeaders,
  //   },
  // });


  // const oamUrl3 = oambaseurl + '/sso/json/sessions';
  // const oamUrl3Query = new URLSearchParams();
  // oamUrl3Query.append('_action', 'getSessionInfo');

  // const oamUrl3Headers = {
  //   ...baseOamAuthenticatedHeaders
  // }

  // const oamUrl3Response = await fetch(oamUrl3 + '?' + oamUrl3Query.toString(), {
  //   method: 'POST',
  //   headers: oamUrl3Headers,
  // });

  // console.log('>> 3', oamUrl3Response.headers.raw()['set-cookie'])

  // const oamUrl3JSONResponse = await oamUrl3Response.json();

  // const fullusername = oamUrl3JSONResponse.username;


  // // This isn't needed either
  // const oamUrl4 = oambaseurl + '/sso/json/realms/root/users/' + fullusername;

  // // console.log(oamUrl4)

  // const oamUrl4Headers = {
  //   ...baseOamAuthenticatedHeaders,
  // };

  // const oamUrl4Response = await fetch(oamUrl4, {
  //   method: 'GET',
  //   headers: oamUrl4Headers,
  // })


  // const oamUrl4JSONResponse = await oamUrl4Response.json();
  // console.log(oamUrl4JSONResponse);

  const myGotoParams = getQueryParamsFromUrl(idpparams.goto);
  // console.log(myGotoParams);

  const oamUrl5 = oambaseurl + updatedGoToPath;

  // console.log('--- oam5\n');
  // console.log(oamUrl5);
  // console.log('---');

  const oamUrl5Query = new URLSearchParams();

  // console.log('myGotoParams',myGotoParams);

  oamUrl5Query.append('acsURL', myGotoParams.acsURL);
  oamUrl5Query.append('index', myGotoParams.index);
  oamUrl5Query.append('ReqID', myGotoParams.ReqID);
  oamUrl5Query.append('binding', myGotoParams.binding);
  oamUrl5Query.append('spEntityID', myGotoParams.spEntityID);

  // console.log('query >>', oamUrl5Query.toString())

  const oamUrl5Headers = {
    // ...baseOamAuthenticatedHeaders,
    'iPlanetDirectoryPro': oamJSONResponse.tokenId,
    'Accept-API-Version': 'resource=2.0, protocol=1.0',
    'Content-Type': 'application/json',
    'Accept': 'text/html,application/xhtml+xml,application/xml;q=0.9,*/*;q=0.8',
  };

  // console.log(oamUrl5Headers);

  // console.log(myGotoParams.binding)

  // console.log('oamUrl5', oamUrl5);

  const oamUrl5Response = await fetch(oamUrl5, {
    method: 'GET',
    headers: oamUrl5Headers,
  })

  // console.log('>> 4', oamUrl4Response.headers.raw()['set-cookie']);
  // console.log('>> 5', oamUrl5Response.headers.raw()['set-cookie']);

  const oamUrl5BodyResponse = await oamUrl5Response.text();

  // console.log(oamUrl5BodyResponse);

  const dom = new JSDOM(oamUrl5BodyResponse);
  const document = dom.window.document;

  let OAMassertion;
  let OAMrelaystate;

  const SAMLResponseObject = document.getElementsByName('SAMLResponse');
  if (SAMLResponseObject.length > 0) {
    OAMassertion = SAMLResponseObject[0].value;
  }

  const RelayStateObject = document.getElementsByName('RelayState');
  if (RelayStateObject.length > 0) {
    OAMrelaystate = RelayStateObject[0].value;
  }

  console.log('RelayState >>', OAMrelaystate);
  
  const ADPayload = new URLSearchParams();
  ADPayload.append('SAMLResponse', OAMassertion);
  if (OAMrelaystate) {
    ADPayload.append('RelayState', OAMrelaystate);
  }

  console.log('******************');
  // console.log(Buffer.from(OAMassertion, 'base64').toString());
  console.log('******************');
  
  const ADResponse = await fetch(idpurl, {
    method: 'POST',
    body: ADPayload,
    headers: {
      // ...baseOamAuthenticatedHeaders,
      // ...oamUrl5Headers,
      // ...ADPayload.getHeaders(),
      // 'Content-Type': 'application/x-www-form-urlencoded',
    },
  });

  // // console.log(ADResponse.header  s);

  const ADBodyResponse = await ADResponse.text();

  // console.log(ADBodyResponse);

  let adSamlResponse;

  const adDom = new JSDOM(ADBodyResponse);
  const adDocument = adDom.window.document;
  const adSamlResponseObject = adDocument.getElementsByName('SAMLResponse');
  if (adSamlResponseObject.length > 0) {
    adSamlResponse = adSamlResponseObject[0].value;
  }

  

  console.log('******************');
  // console.log(Buffer.from(adSamlResponse, 'base64').toString());
  console.log('******************');
  
  // console.log(adSamlResponse);

  const AWSPayload = new URLSearchParams();
  AWSPayload.append('SAMLResponse', adSamlResponse);

  const awsformsubmiturl = 'https://signin.aws.amazon.com/saml';
  const AWSResponse = await fetch(awsformsubmiturl, {
    method: 'POST',
    body: AWSPayload,
  })

  const awsBodyResponse = await AWSResponse.text();
  // console.log(awsBodyResponse);

  let awsSamlResponse;
  
  const awsDom = new JSDOM(awsBodyResponse);
  const awsDocument = awsDom.window.document;
  const awsSamlResponseObject = awsDocument.getElementsByName('SAMLResponse');
  if (awsSamlResponseObject.length > 0) {
    awsSamlResponse = awsSamlResponseObject[0].value;
  }

  
  // console.log(Buffer.from(awsSamlResponse, 'base64').toString());
  
  const root = await xml2js.parseStringPromise((Buffer.from(awsSamlResponse, 'base64').toString()));
  const attributeStatement = root['samlp:Response']['Assertion'][0]['AttributeStatement'][0]['Attribute'];
  const rolesAttribute = attributeStatement.find((attr) => attr['$']['Name'] === 'https://aws.amazon.com/SAML/Attributes/Role');
  
  // console.log(rolesAttribute['AttributeValue'])
  
  const awsRoles = [
    ...rolesAttribute['AttributeValue'].reduce((acc, val) => {
      const [principal_arn, role_arn] = val.split(',');
      return [
        ...acc,
        {
          role_arn,
          principal_arn,
        }
      ]
    }, [])
  ];
  
  
  const samlAccountEls = awsDocument.getElementsByClassName('saml-account-name');
  const samlAccountNames = [];
  
  for (let i = 0; i < samlAccountEls.length; i++) {
    samlAccountNames.push(samlAccountEls.item(i).textContent);
    console.log(`[${i}] ${samlAccountNames[i]}`);
  }
  
  console.log(awsRoles)
  

  console.log(awsRoles[0]);
  
  const sts = new AWS.STS();
  sts.assumeRoleWithSAML({
    DurationSeconds: 3600, 
    PrincipalArn: awsRoles[0].principal_arn, 
    RoleArn: awsRoles[0].role_arn, 
    SAMLAssertion: awsSamlResponse,
  }, console.log);


  

  // console.log(attributeStatement);

  // console.log(JSON.stringify(attributeStatement));


  // console.log(root.write());
  // const saml2attribute = 
  // root.find("Assertion[@xmlns='urn:oasis:names:tc:SAML:2.0:assertion']").toString();
  // console.log(saml2attribute.get('Name'));
  // console.log(saml2attribute);
  // console.log(root.findall("Attribute[@name='https://aws.amazon.com/SAML/Attributes/RoleSessionName']"));
  // console.log(root.findall("Attribute[@name='https://aws.amazon.com/SAML/Attributes/Role']"));

  // console.log(root.findall("*/assertion"))
  // console.log(root.findtext('{urn:oasis:names:tc:SAML:2.0:assertion}Attribute'))

})()