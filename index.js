#!/usr/bin/env node

const Configstore = require('configstore');
const inquirer = require('inquirer');
const clear = require('clear');

const packageJson = require('./package.json');
const { asciiLogo } = require('./asciiArt');

const fetchExisting = require('./lib/fetchExisting');

const { printTable, Table } = require('console-table-printer');
const moment = require('moment');

const schoolTable = require('./lib/schoolTable');

const awsAuthentication = require('./lib/awsAuthentication');

const config = new Configstore(packageJson.name, {});
const Spinner = require('cli-spinner').Spinner;

let selectedEnvironemnt = undefined;

// config.clear();

let updates = [];


const setupScreen = require('./screens/setup');
const selectEnvironementScreen = require('./screens/selectEnvironment');
const homeMenuScreen = require('./screens/homeMenu');
const makeOperationalScreen = require('./screens/makeOperational');
const makeNonOperationalScreen = require('./screens/makeNonOperational');
const commitScreen = require('./screens/commit');
const settingsScreen = require('./screens/settings');

async function startPage() {
  clear();
  console.log(asciiLogo);
  console.log(`NSW School Update CLI - v${packageJson.version}\n`);

  if (selectedEnvironemnt) {
    printTable([ { ["Environment"]: selectedEnvironemnt.name || selectedEnvironemnt.key, ["Schools Table Name"]: selectedEnvironemnt.schoolsTableName } ])
    console.log(`\n`);
  }
}

async function loadSchoolsFromServer() {
  startPage();
  const spinner = new Spinner('Loading Schools');
  spinner.setSpinnerString(18);
  spinner.start();
  await loadingSchools();
  spinner.stop();
}

async function start() {
  const environemnts = config.get('env') || {};

  startPage();

  const { name, accessKeyId, secretAccessKey, sessionToken } = await awsAuthentication({});

  console.log(accessKeyId, secretAccessKey);

  selectedEnvironemnt = {
    name,
    accessKeyId: accessKeyId,
    accessKeySecret: secretAccessKey,
    sessionToken: sessionToken,
    region: 'ap-southeast-2',
    proxyAddress: '',
    schoolsTableName: 'nswschoolsafety-db-public-schools',
    // proxyAddress: selectedEnvironemnt.proxyAddress,
  }


  // if (!Object.keys(environemnts).length) {
  //   const selectedEnvironmentType = await setupScreen();
  //   selectedEnvironemnt = config.get(`env.${selectedEnvironmentType}`);
  //   selectedEnvironemnt.key = selectedEnvironmentType;
  // } else {
  //   const selectedEnvironmentType =  await selectEnvironementScreen();
  //   selectedEnvironemnt = config.get(`env.${selectedEnvironmentType}`);
  //   selectedEnvironemnt.key = selectedEnvironmentType;
  // }

  await loadSchoolsFromServer();
  mainMenu();
}

function getConnectionInformation () {
  return {
    accessKeyId: selectedEnvironemnt.accessKeyId,
    accessKeySecret: selectedEnvironemnt.accessKeySecret,
    sessionToken: selectedEnvironemnt.sessionToken,
    region: selectedEnvironemnt.region,
    proxyAddress: '',
    tableName: selectedEnvironemnt.schoolsTableName,
    proxyAddress: selectedEnvironemnt.proxyAddress,
  }
}

async function loadingSchools() {
  const nonOperationalSchools = await fetchExisting(getConnectionInformation());

  updates = [];

  if (nonOperationalSchools && nonOperationalSchools.Items.length > 0) {
    updates = nonOperationalSchools.Items.filter((item) => item.nonOperational);
  }
}



async function mainMenu () {
  startPage();
  
  schoolTable(updates);
  console.log('\n------------------------------------------------------------------------------------\n');

  const option = await homeMenuScreen();

  switch(option) {
    case 'makeOperational':
      updates = await makeOperationalScreen(updates, getConnectionInformation());
      break;
    case 'makeNonOperational':
      updates = await makeNonOperationalScreen(updates, getConnectionInformation());
      break;
    case 'getCurrent':
      await loadSchoolsFromServer();
      break;
    case 'commit':
      const confirm = await commitScreen(updates, getConnectionInformation());

      if (confirm) {
        await loadSchoolsFromServer();
      }
      break;
    case 'settings':
      await settingsScreen();
      break;
    default:
      break;
  }

  mainMenu();
}

start();
